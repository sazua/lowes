//
//  CustomerCollectionViewCell.swift
//  Lowes Instant Help
//
//  Created by Sergio Azua on 1/23/16.
//  Copyright © 2016 Sergio Azua. All rights reserved.
//

import UIKit

class CustomerCollectionViewCell: UICollectionViewCell {

    @IBOutlet var nameLbl: UILabel!
    var customer: Customer! {
        didSet{
            nameLbl.text = customer.customerName
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func formatConversation()
    {
        
    }

}
