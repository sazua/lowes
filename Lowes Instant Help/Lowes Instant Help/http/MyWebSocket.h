#import <Foundation/Foundation.h>
#import "WebSocket.h"

@interface Message: NSObject
@property (nonatomic, strong) NSString *msg;
@property (nonatomic, strong) NSDate *timeStamp;
@end

@implementation Message

@end

@interface MyWebSocket : WebSocket
{
    
}


@property (nonatomic, strong) NSMutableArray *messagesSent;
@property (nonatomic, strong) NSMutableArray *messagesReceived;

@end
