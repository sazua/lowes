#import "MyWebSocket.h"
#import "HTTPLogging.h"
// Log levels: off, error, warn, info, verbose
// Other flags : trace
static const int httpLogLevel = HTTP_LOG_LEVEL_WARN | HTTP_LOG_FLAG_TRACE;


@implementation MyWebSocket


-(id)init
{
    self = [super init];
    
    if (self)
    {
        _messagesReceived = [NSMutableArray new];
        _messagesSent = [NSMutableArray new];
    }
    
    return self;
}

- (void)didOpen
{
	HTTPLogTrace();
	
	[super didOpen];
   }

- (void)didReceiveMessage:(NSString *)msg
{
    [super didReceiveMessage:msg];
    [_messagesReceived addObject:msg];
    
}

- (void)sendMessage:(NSString *)msg
{
    [super sendMessage:msg];
    [_messagesSent addObject:msg];
}
- (void)didClose
{
	HTTPLogTrace();
	
	[super didClose];
}

@end
