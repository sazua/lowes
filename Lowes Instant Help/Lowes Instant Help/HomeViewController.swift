//
//  ViewController.swift
//  Lowes Instant Help
//
//  Created by Sergio Azua on 1/23/16.
//  Copyright © 2016 Sergio Azua. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, WebSocketDelegate {
    @IBOutlet var collectionView: UICollectionView!
    
    
    var customers = Array<Customer>()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.title = "Lowe's Instant Help"
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("wsAdded:"), name: SOCKET_ADDED_NOTIFICATION, object: nil)
        self.collectionView.registerNib(UINib(nibName: "CustomerCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CustomerCollectionViewCell")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func sendHi(sender: UIButton)
    {
        let del = UIApplication.sharedApplication().delegate as! AppDelegate
        for var i = 0; i < del.httpServer.webSockets.count ; i++
        {
            let socket = del.httpServer.webSockets[i] as! MyWebSocket
        }
        
    }
    
    
    func wsAdded(not: NSNotification)
    {
        let myWs = not.object as! MyWebSocket
        
        myWs.delegate = self
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if customers.count == 0
        {
            return 0
        }
        
        if customers.count % 2 == 0
        {
            return 2
        }else
        {
            return  1
        }
        
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int
    {
        if customers.count == 0
        {
            return 0
        }
        
        if customers.count % 2 == 0
        {
            return customers.count / 2
        }else
        {
            return  (customers.count / 2) + 1
        }

    }
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("CustomerCollectionViewCell", forIndexPath: indexPath) as! CustomerCollectionViewCell
        
        var idx = indexPath.section * 2 + indexPath.row
        cell.customer = customers[idx]
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake(self.view.frame.width / 2 - 5, self.view.frame.width / 2 - 5)
    }
    
    func webSocketDidOpen(ws: WebSocket!) {
        
    }
    
    func webSocketDidClose(ws: WebSocket!) {
        
        var customerLeaving: Customer?
        for customer in customers{
            
            if customer.webSocket === ws{
                customerLeaving = customer
                break
            }
        }
        
        if let cust = customerLeaving
        {
            customers.removeAtIndex(customers.indexOf(cust)!)
        }
    }
    
    func webSocket(ws: WebSocket!, didReceiveMessage msg: String!) {

        if msg.containsString("name=")
        {
            let oCMsg = msg as NSString
            let name = oCMsg.substringFromIndex(oCMsg.rangeOfString("name=").length)
            let newCustomer = Customer(name: name, ws: ws as! MyWebSocket)
            
            customers.append(newCustomer)
            
            
            dispatch_async(dispatch_get_main_queue(), {
                self.collectionView.reloadData()
            })
        }
    
    }
}

