//
//  Customer.swift
//  Lowes Instant Help
//
//  Created by Sergio Azua on 1/23/16.
//  Copyright © 2016 Sergio Azua. All rights reserved.
//

import UIKit

class Customer: NSObject {

    let customerName: String!
    let webSocket : MyWebSocket!
    init(name: String, ws: MyWebSocket) {
        customerName = name
        webSocket = ws
    }
}
